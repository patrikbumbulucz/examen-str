import java.util.concurrent.locks.Lock;
import java.util.concurrent.Semaphore;
import java.util.Random;
import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;
public class ExecutionThread1 extends Thread {
    Lock l1;
    Semaphore s1;
    CyclicBarrier cb;
    int sleep_min, sleep_max, activity_min, activity_max;

    public ExecutionThread(Lock l1, Semaphore s1, CyclicBarrier cb, int sleep_min, int sleep_max, int activity_min, int activity_max) {
        this.l1 = l1;
        this.s1 = s1;
        this.cb = cb;
        this.sleep_min = sleep_min;
        this.sleep_max = sleep_max;
        this.activity_min = activity_min;
        this.activity_max = activity_max;
    }

    public void run() {
        while (true) {
            System.out.println(this.getName() + " - STATE 1");
            int k = (int) Math.round(Math.random() * (activity_max - activity_min) + activity_min);
            for (int i = 0; i < k * 100000; i++) {
                i++;
                i--;
            }
            l1.lock();
            System.out.println(this.getName() + " - STATE 2");
            int k = (int) Math.round(Math.random() * (activity_max - activity_min) + activity_min);
            for (int i = 0; i < k * 100000; i++) {
                i++;
                i--;
            }
            s1.acquire(1);
            System.out.println(this.getName() + " - STATE 3");
            try {
                Thread.sleep(Math.round(Math.random() * (sleep_max - sleep_min) + sleep_min) * 500);
            } catch (InterruptedException e) {
                e.printStackTrace();                                                              //
            }
            s1.release(1);
            l1.unlock();
            System.out.println(this.getName() + " - STATE 4");
            cb.await();
        }
    }
}

public class ExecutionThread2 extends Thread {
    Lock l1;
    Semaphore s1;
    CyclicBarrier cb;

    int sleep_min, sleep_max, activity_min, activity_max;

    public ExecutionThread(Lock l1, Semaphore s1, CyclicBarrier cb, int sleep_min, int sleep_max, int activity_min, int activity_max) {
        this.l1 = l1;
        this.s1 = s1;
        this.cb = cb;
        this.sleep_min = sleep_min;
        this.sleep_max = sleep_max;
        this.activity_min = activity_min;
        this.activity_max = activity_max;
    }

    public void run() {
        while(true) {
            System.out.println(this.getName() + " - STATE 1");
            int k = (int) Math.round(Math.random() * (activity_max - activity_min) + activity_min);
            for (int i = 0; i < k * 100000; i++) {
                i++;
                i--;
            }
            s1.acquire(1);
            System.out.println(this.getName() + " - STATE 2");
            int k = (int) Math.round(Math.random() * (activity_max - activity_min) + activity_min);
            for (int i = 0; i < k * 100000; i++) {
                i++;
                i--;
            }
            l1.lock();
            System.out.println(this.getName() + " - STATE 3");
            try {
                Thread.sleep(Math.round(Math.random() * (sleep_max - sleep_min) + sleep_min) * 500);
            } catch (InterruptedException e) {
                e.printStackTrace();                                                              //
            }
            s1.release(1);
            l1.unlock();
            System.out.println(this.getName() + " - STATE 4");
            cb.await();


        }
    }
}



public class Main {
    public static void main(String[] args) {
        CyclicBarrier cb = new CyclicBarrier(2);
        Lock b1=new ReentrantLock();
        Semaphore b2=new Semaphore(1);
        new ExecutionThread1(2, 4, 3, 6).start();
        new ExecutionThread2(3, 5, 4, 7).start();
        new ExecutionThread3(3, 5, 4, 7).start();
    }
}
