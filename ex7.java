import java.util.Random;
import java.util.concurrent.CountDownLatch;

public class ExecutionThread1 extends Thread {
    Integer a1;
    Integer a2;
    CountDownLatch cd1;
    int sleep_min, sleep_max, activity_min, activity_max;

    public ExecutionThread(CountDownLatch cd1, Integer a1, Integer a2, int sleep_min, int sleep_max, int activity_min, int activity_max) {
        this.a1 = a1;
        this.a2 = a2;
        this.cd1 = cd1;
        this.sleep_min = sleep_min;
        this.sleep_max = sleep_max;
        this.activity_min = activity_min;
        this.activity_max = activity_max;
    }

    public void run() {
        System.out.println(this.getName() + " - STATE 1");
        try {
            Thread.sleep(Math.round(Math.random() * (sleep_max - sleep_min)+ sleep_min) * 500);
        } catch (InterruptedException e) {
            e.printStackTrace();                                                              //
        }
        System.out.println(this.getName() + " - STATE 2");
        int k = (int) Math.round(Math.random()*(activity_max - activity_min) + activity_min);
        for (int i = 0; i < k * 100000; i++) {
            i++; i--;
        }
        a1.notify();
        a2.notify();
        System.out.println(this.getName() + " - STATE 3");
        cd1.countDown();
        cd1.await();

    }

    }

    public class ExecutionThread2 extends Thread {
        Integer a1;
        CountDownLatch cd1;
        int sleep_min, sleep_max, activity_min, activity_max;

        public ExecutionThread(Integer a1, CountDownLatch cd1, int sleep_min, int sleep_max, int activity_min, int activity_max) {
            this.a1 = a1;
            this.cd1 = cd1;
            this.sleep_min = sleep_min;
            this.sleep_max = sleep_max;
            this.activity_min = activity_min;
            this.activity_max = activity_max;
        }

        public void run() {
            System.out.println(this.getName() + " - STATE 1");
            a1.wait();
            try {
                Thread.sleep(Math.round(Math.random() * (sleep_max - sleep_min)+ sleep_min) * 500);
            } catch (InterruptedException e) {
                e.printStackTrace();                                                              //
            }
            System.out.println(this.getName() + " - STATE 2");
            int k = (int) Math.round(Math.random()*(activity_max - activity_min) + activity_min);
            for (int i = 0; i < k * 100000; i++) {
                i++; i--;
            }
            System.out.println(this.getName() + " - STATE 3");
            cd1.countDown();
            cd1.await();


        }
    }

    public class ExecutionThread3 extends Thread {
        Integer a2;
        CountDownLatch cd1;
        int sleep_min, sleep_max, activity_min, activity_max;

        public ExecutionThread(Integer a2, CountDownLatch cd1, int sleep_min, int sleep_max, int activity_min, int activity_max) {
            this.a2 = a2;
            this.cd1 = cd1;
            this.sleep_min = sleep_min;
            this.sleep_max = sleep_max;
            this.activity_min = activity_min;
            this.activity_max = activity_max;
        }

        public void run() {
            System.out.println(this.getName() + " - STATE 1");
            a2.wait();
            try {
                Thread.sleep(Math.round(Math.random() * (sleep_max - sleep_min)+ sleep_min) * 500);
            } catch (InterruptedException e) {
                e.printStackTrace();                                                              //
            }
            System.out.println(this.getName() + " - STATE 2");
            int k = (int) Math.round(Math.random()*(activity_max - activity_min) + activity_min);
            for (int i = 0; i < k * 100000; i++) {
                i++; i--;
            }
            System.out.println(this.getName() + " - STATE 3");
            cd1.countDown();
            cd1.await();


        }
    }

    public class Main {
        public static void main(String[] args) {
            Integer a1 = new Integer(1);
            Integer a2 = new Integer(2);
            CountDownLatch cd1= new CountDownLatch(3);
            new ExecutionThread1(7, 7, 2, 3).start();
            new ExecutionThread2(5, 5, 3, 5).start();
            new ExecutionThread3(5, 5, 4, 6).start();
        }
    }